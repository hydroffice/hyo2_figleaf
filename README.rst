Fig Leaf tool
=============

.. image:: https://github.com/hydroffice/hyo2_figleaf/raw/master/hyo2/figleaf/app/media/app_icon.png
    :alt: logo

|

.. image:: https://img.shields.io/badge/docs-latest-brightgreen.svg
    :target: https://www.hydroffice.org/manuals/figleaf/index.html
    :alt: Latest Documentation

.. image:: https://api.codacy.com/project/badge/Grade/3a645cbbdd814838a41312ed1fc578be
    :target: https://www.codacy.com/app/hydroffice/hyo2_figleaf/dashboard
    :alt: codacy

|

* GitHub: `https://github.com/hydroffice/hyo2_figleaf <https://github.com/hydroffice/hyo2_figleaf>`_
* Project page: `url <https://www.hydroffice.org/figleaf>`_
* License: LGPLv3 or IA license (See `Dual license <https://www.hydroffice.org/license/>`_)

|

General info
------------

The Fig Leaf package is a library and an app for manipulation of raster data.

|

Credits
-------

Authors
~~~~~~~

This code is written and maintained by:

- `Giuseppe Masetti <mailto:gmasetti@ccom.unh.edu>`_
- `Glen Rice <mailto:glen.rice@noaa.gov>`_


Contributors
~~~~~~~~~~~~

The following wonderful people contributed directly or indirectly to this project:

- `John Doe <mailto:john.doe@email.me>`_

Please add yourself here alphabetically when you submit your first pull request.
